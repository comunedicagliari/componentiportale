
# Bundle aggiuntivo POM-SHARE al bundle del Comune di Cagliari 
Questo codice sorgente permette di installare la funzionalità di ``social sharing`` presente nelle pagine di dettaglio di determinati contenuti. 

## Introduzione ed utilizzo

Questo bundle va a modificare 2 ``tipi di contenuto`` presenti nel template di base del ``Comune di Cagliari``.
I tipi di contenuto che vengono modificati sono ``NOV`` e ``SRV``. 
In ognuno di questi vengono modificati 3 attributi presenti andando ad assegnare i ``ruoli`` che seguono:

1. social:title - assegnato all'attributo con codice ``title``
2. social:description - assegnato all'attributo con codice ``subtitle``
3. social:image - assegnato all'attributo con codice ``img`` (N.B. questo ruolo può essere assegnato solo ad un attributo di tipo immagine)

Ovviamente i ruoli assegnati ai vari attributi posso essere modificati a piacimento recandosi nella sezione  ``contenuti`` -> ``tipi`` e selezionando il tipo di contenuto al quale si vuole attribuire la funzionalità di social sharing.
Nel bundle sono compresi anche 4 ``modelli di contenuto``:

1. Card interna con share codice NOV
2. Card servizi con share codice SRV 
3. Dettaglio novità share codice NOV
4. Dettaglio servizi share codice SRV

che permettono di sfruttare la funzionalità dello ``sharing`` in ``modalità dettaglio``.

Infine un nuovo modello di pagina ``pom_template_detail_share`` viene aggiunto.
Questo è modello di pagina è fondamentale per attivare e valorizzare i campi ``meta tag`` posizionati all'interno della sezione ``<head>`` necessari per lo sharing dei contenuti sui social.
Al suo interno infatti troviamo la logica necessaria disponibile out of the box che può essere customizzata a discrezione dello sviluppatore.


## Build e deploy manuale del bundle (Developers)

1. Prerequisto fondamentale - intallare ``Entando CLI`` e  collegarlo all'istanza di Entando in cui si vuole installare il bundle (da fare solo la prima volta). Controllare la sezione ``Approfondimenti`` in basso.
2. entrare nella cartella del bundle desiderato
3. da terminale digitare `ent bundle pack` che preparerà il pacchetto di installazione del bundle
4. poi digitare `ent bundle publish` che provvederà a creare e pubblicare l'immagine docker su un repository Docker. N.B. un account docker è fondamentale per la riuscita della pubblicazione del bundle 
5. poi digitare `ent bundle deploy`  che renderà disponibile il bundle nel portale Entando pronto per l'installazione tramite ``HUB`` 
6. digitare infine `ent bundle install` se si vuole installare direttamente il bundle senza passare per l' ``HUB``

## Installazione del bundle tramite Entando HUB (Utente normale)

1. Accedi alla tua istanza di Entando
2. Vai su `Hub` e cliccaci sopra 
3. Vedrai una lista di ``bundles`` che posso essere installati
4. Fare clic sul pulsante ``Distribuito`` su ``pom-share``
5. Non appena la finestra modale si apre cliccare sul bottone ``installa``
6. Un'altra finestra modale si aprirà in questo caso cliccate prima sul bottone ``aggiorna tutto`` e sul bottone ``ok``
7. Aspetta che il ``bundle`` termini l'installazione
8. Chiudere pure la finestra modale in caso non si siano evidenziati errori durante l'installazione
9. Utilizzate il widget in ogni pagina ove sia necessario o modificatelo in base alle necessità progettuali.

# Approfondimenti (developers)

## Installazione della CLI 
```https://developer.entando.com/next/docs/getting-started/entando-cli.html#install-the-cli```

## Gestione bundle e comandi della CLI 
```https://developer.entando.com/next/docs/curate/bundle-details.html#entando-bundle-conventions```

## Struttura del bundle 
```https://developer.entando.com/next/docs/curate/bundle-details.html```

## Hub configurazione e installazione
Per comprendere a fondo come utilizzare Hub, fate riferimento a ```https://developer.entando.com/v7.0/tutorials/solution/entando-hub.html#overview```
