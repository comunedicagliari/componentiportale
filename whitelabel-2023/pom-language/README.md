
# Bundle aggiuntivo POM-LANGUAGE al bundle del Comune di Cagliari 
Questo codice sorgente permette di installare la dropdown per la selezione della lingua. 

## Introduzione

Questo bundle installa un singolo widget di scelta della lingua che può essere utilizzato all'interno di ogni pagina del portale nella sezione del masthead.

## Build e deploy manuale del bundle (Developers)

1. Prerequisto fondamentale - intallare ``Entando CLI`` e  collegarlo all'istanza di Entando in cui si vuole installare il bundle (da fare solo la prima volta). Controllare la sezione ``Approfondimenti`` in basso.
2. entrare nella cartella del bundle desiderato
3. da terminale digitare `ent bundle pack` che preparerà il pacchetto di installazione del bundle
4. poi digitare `ent bundle publish` che provvederà a creare e pubblicare l'immagine docker su un repository Docker. N.B. un account docker è fondamentale per la riuscita della pubblicazione del bundle 
5. poi digitare `ent bundle deploy`  che renderà disponibile il bundle nel portale Entando pronto per l'installazione tramite ``HUB`` 
6. digitare infine `ent bundle install` se si vuole installare direttamente il bundle senza passare per l' ``HUB``

## Installazione del bundle tramite Entando HUB (Utente normale)

1. Accedi alla tua istanza di Entando
2. Vai su `Hub` e cliccaci sopra 
3. Vedrai una lista di ``bundles`` che posso essere installati
4. Fare clic sul pulsante ``Distribuito`` su ``pom-language``
5. Non appena la finestra modale si apre cliccare sul bottone ``installa``
6. Un'altra finestra modale si aprirà in questo caso cliccate prima sul bottone ``aggiorna tutto`` e sul bottone ``ok``
7. Aspetta che il ``bundle`` termini l'installazione
8. Chiudere pure la finestra modale in caso non si siano evidenziati errori durante l'installazione
9. Utilizzate il widget in ogni pagina ove sia necessario o modificatelo in base alle necessità progettuali.

# Approfondimenti (developers)

## Installazione della CLI 
```https://developer.entando.com/next/docs/getting-started/entando-cli.html#install-the-cli```

## Gestione bundle e comandi della CLI 
```https://developer.entando.com/next/docs/curate/bundle-details.html#entando-bundle-conventions```

## Struttura del bundle 
```https://developer.entando.com/next/docs/curate/bundle-details.html```

## Hub configurazione e installazione
Per comprendere a fondo come utilizzare Hub, fate riferimento a ```https://developer.entando.com/v7.0/tutorials/solution/entando-hub.html#overview```
