**Introduzione**

All’interno di questa sezione sono presenti le seguenti cartelle:

- Comune-ca, il modello di base che raccoglie i principali elementi da cui partire per la realizzazione di un nuovo portale
- Pom-bundle-sharing, contiene la funzionalità di social sharing
- Pom-carousel,  contiene un set di widget per la creazione di carousel.
- Pom-cerca-standard, contiene la barra del motore di ricerrca
- Pom-cookie,	contiene un set di widget per la gestione delle cookie policy
- Pom-language, contiene la funzionalità per la scelta della lingua del portale
- Pom-masonry, contiene una griglia di tipo masonry oppure una griglia sfalsata.
- Pom-navigazione, raccoglie un set di widget per la costruzione di menù dinamici.
- Pom-proportional-gallery, contiene widget e modelli per realizzare gallerie di contenuti.

Ciascuna cartella contiene dei bundle. I bundle rappresentano l’unità di distribuzione all'interno di un'applicazione  
Entando. Un bundle può contenere uno o più componenti, come micro frontend, microservizi o altre tipologie di componenti  
quali widgets, fragments, modelli di pagina, tipi di contenuto ecc.
Attraverso l’utilizzo dei bundle, Entando offre un modello  
d'installazione e distribuzione che si basa su un approccio modulare e componibile. Questo modello offre numerosi vantaggi,  
tra cui l’aumento della produttività, l’armonizzazione 
dei modelli di distribuzione e la standardizzazione delle architetture. Inoltre,  
il modello può essere facilmente esteso per lo scambio, la condivisione ed il riuso tra Pubbliche Amministrazioni.

**Prerequisiti**

Per utilizzare questi bundle è necessaria disporre di una istanza Entando 7.x che può essere scaricata in [questa pagina.](https://developer.entando.com/)
