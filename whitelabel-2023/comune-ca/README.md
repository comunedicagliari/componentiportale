
# Bundle template di base del Comune di Cagliari 
Il bundle ``comune-ca`` rappresenta il template aggiornato del Comune di Cagliari. Questo bundle è quindi riutilizzabile e deve essere utilizzato come punto di partenza per progetti basati su amministrazioni comunali o PA italiane.

## Introduzione e riepilogo

Il bundle ``comune-ca`` installa una serie di componenti funzionali, una parte statica ed una parte dinamica, funzionale all'intera realizzazione di un portale.
Il descrittore di questo bundle è sato realizzato utilizzando il ``descriptorVersion: v5`` e solo tramite questa schema il template di base può essere modificato.
per ulteriori approfondimenti sul descrittore v5 consultare la pagina ```https://developer.entando.com/next/docs/reference/bundle-comparison.html```
Una volta installato l'utente avrà a disposizione un mini portale comprensivo di pagine e contenuti di esempio completamente modificabili.
Tra i componenti che vengono installati troviamo:

1. assets - lista di immagini e documenti di esempio

2. categorie - categorie di default presnti nel portale 

3. contentModels - modelli di visualizzazione per contenuti:
     - argomenti-in-evidenza-homepage-(arg)
     - argomenti-in-pillole-(arg)
     - card-amministrazione-generica-(cra)
     - card-interna-novita-immagine-(nov)
     - card-novita-evidenza-(nov)
     - card-singola-servizi-(srv)
     - dettaglio-servizi-(srv)
     - intestazione-pa-con-logo-(ttl)
     - intestazione-pa-con-logo-footer-(ttl)
     - link-+-icone-footer-(lst)
     - link-card-amministrazione-(cra)
     - link-card-novita-(crn)
     - lista-di-link-esterni-(exl)
     - lista-link-footer-bar-(lst)
     - lista-link-footer-titolo-e-textarea(lst)
     - novita-card-dettaglio-(nov)
     - novita-card-singole-(nov)
     - novita-in-evidenza-homepage-(nov)
     - titolo-link-+-lista-link-footer-(lst)
     - titolo-link-+-textarea-+-lista-link-footer-(lst)
     - titolo-link-+-textarea-footer-(lst)

4. contentTypes - strutture necessarie per creazione di contenuti:
    - contentTypes/agn
    - contentTypes/arg
    - contentTypes/cra
    - contentTypes/crn
    - contentTypes/exl
    - contentTypes/lst
    - contentTypes/nov
    - contentTypes/srv
    - contentTypes/ttl

5. fragments - frammenti di codice funzionali ai modelli di pagine o plugin

6. labels - etichette di sistema pre-caricat nel sistema 

7. pageModels - template di pagina basati sulla libreria ``Bootstrap Italia 1.6.4``:
     - pom_template
     - pom_template_detail
     - pom_template_internal-base
     - pom_template_internal

8. risorse - cartelle che contengono tutte le libreie necessarie per il funzionamento delle pagine e dei widget del portale

9. widgets - lista di widget statici e dinamici che sono compresi nel template di base. Sono modificabili o rimpiazzabili da altri widget installabili da bundle aggiuntivi:     
    - widgets/pom_1_menu
    - widgets/pom_2_menu
    - widgets/pom_3_menu
    - widgets/pom_breadcrumb
    - widgets/pom_carousel
    - widgets/pom_contacts_footer
    - widgets/pom_content_search_query
    - widgets/pom_content_single
    - widgets/pom_docs_footer
    - widgets/pom_extralinks_footer
    - widgets/pom_follow_footer
    - widgets/pom_followus
    - widgets/pom_highlights_administration
    - widgets/pom_home_argomenti_evidenza_lista
    - widgets/pom_home_pubblica_lista
    - widgets/pom_home_singolo_contenuto
    - widgets/pom_log_spid
    - widgets/pom_login_form
    - widgets/pom_logo_comune
    - widgets/pom_logo_comune_footer
    - widgets/pom_menu_amm
    - widgets/pom_news_footer
    - widgets/pom_notizie_all_lista
    - widgets/pom_notizie_highlight_lista
    - widgets/pom_pagamenti_highlight_lista
    - widgets/pom_pubblica_lista
    - widgets/pom_related_administration_news
    - widgets/pom_search
    - widgets/pom_services_footer
    - widgets/pom_servizi_all_lista
    - widgets/pom_servizi_highlight_lista
    - widgets/pom_transp_footer
    - widgets/pom_urbanistica_catasto_all_lista
    - widgets/pom_urbanistica_catasto_evidenza_lista

10. pages - Lista di pagine presenti nel bundle
    - pages/administration
    - pages/calendario
    - pages/documents_and_data
    - pages/homepage
    - pages/news
    - pages/risultato_ricerca
    - pages/servizi
    - pages/topics
    - pages/xa-bulletin
    - pages/xb-cantieri
    - pages/xc-catasto_urbanistica
    - pages/xd-comunicati
    - pages/xe-council
    - pages/xf-cultura
    - pages/xg-dettagli_servizi
    - pages/xh-detail
    - pages/xi-dettagli_news
    - pages/xl-dettaglio_novita
    - pages/xm-latest_news
    - pages/xn-luoghi
    - pages/xo-notices
    - pages/xp-payments
    - pages/xq-servizi_anagrafe
    - pages/xr-sport
    - pages/xs-uffici
    - pages/xt-vita_lavorativa


## Build e deploy manuale del bundle (Developers)

1. Prerequisto fondamentale - intallare ``Entando CLI`` e  collegarlo all'istanza di Entando in cui si vuole installare il bundle (da fare solo la prima volta). Controllare la sezione ``Approfondimenti`` in basso.
2. entrare nella cartella del bundle desiderato
3. da terminale digitare `ent bundle pack` che preparerà il pacchetto di installazione del bundle
4. poi digitare `ent bundle publish` che provvederà a creare e pubblicare l'immagine docker su un repository Docker. N.B. un account docker è fondamentale per la riuscita della pubblicazione del bundle 
5. poi digitare `ent bundle deploy`  che renderà disponibile il bundle nel portale Entando pronto per l'installazione tramite ``HUB`` 
6. digitare infine `ent bundle install` se si vuole installare direttamente il bundle senza passare per l' ``HUB``

## Installazione del bundle tramite Entando HUB (Utente normale)

1. Accedi alla tua istanza di Entando
2. Vai su `Hub` e cliccaci sopra 
3. Vedrai una lista di ``bundles`` che posso essere installati
4. Fare clic sul pulsante ``Distribuito`` su ``comune-ca``
5. Non appena la finestra modale si apre cliccare sul bottone ``installa``
6. Un'altra finestra modale si aprirà in questo caso cliccate prima sul bottone ``aggiorna tutto`` e sul bottone ``ok``
7. Aspetta che il ``bundle`` termini l'installazione
8. Chiudere pure la finestra modale in caso non si siano evidenziati errori durante l'installazione
9. Utilizzate il widget in ogni pagina ove sia necessario o modificatelo in base alle necessità progettuali.

# Approfondimenti (developers)

## Installazione della CLI 
```https://developer.entando.com/next/docs/getting-started/entando-cli.html#install-the-cli```

## Gestione bundle e comandi della CLI 
```https://developer.entando.com/next/docs/curate/bundle-details.html#entando-bundle-conventions```

## Struttura del bundle 
```https://developer.entando.com/next/docs/curate/bundle-details.html```

## Hub configurazione e installazione
Per comprendere a fondo come utilizzare Hub, fate riferimento a ```https://developer.entando.com/v7.0/tutorials/solution/entando-hub.html#overview```
