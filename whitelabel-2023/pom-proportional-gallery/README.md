
# Bundle aggiuntivo POM-GALLERIE al bundle del Comune di Cagliari 
Questo bundle permette di installare una serie di widget e modelli per realizzare gallerie di contenuti. 

## Introduzione ed utilizzo

Questo bundle aggiunge 2 wigdet per gallerie contenuti non presenti nel template di base.
Vengono installati un widget per la una normale galleria ed un'latro per gallerie di immagini con griglia proporzionale. Per ulteriori informazioni ed esempi consultare  ```https://italia.github.io/bootstrap-italia/1.x/docs/organizzare-i-contenuti/liste-di-immagini/#standard-con-didascalie``` e ```https://italia.github.io/bootstrap-italia/1.x/docs/organizzare-i-contenuti/liste-di-immagini/#proporzionale```.
Vengono inoltre aggiunti 6 ``modelli di contenuto`` e modficati 3 ``tipi di contenuto`` per utilizzare ognuna delle 2 gallerie.
I tipi di contenuto che vengono modificati sono ``MED``,``NOV`` e ``SRV``. 
Ricordate che quando vengono modificati i ``Tipi di Contenuto`` à necessario aggiornare le referenze. Le referenze si aggiornano andando nella sezione ``contenuti`` -> ``tipi di contenuto``.
I widget possono essere ulteriormente modificati a livello applicativo nella sezione nel backoffice di Entando cliccando su ``componenti`` -> ``fragments``  e scegliendo di modificare a piacimento i ``fragment`` che li compongono effettuando una ricerca per codice:

1. pom_caption_gallery
2. pom_poportional_gallery

## Build e deploy manuale del bundle (Developers)

1. Prerequisto fondamentale - intallare ``Entando CLI`` e  collegarlo all'istanza di Entando in cui si vuole installare il bundle (da fare solo la prima volta). Controllare la sezione ``Approfondimenti`` in basso.
2. entrare nella cartella del bundle desiderato
3. da terminale digitare `ent bundle pack` che preparerà il pacchetto di installazione del bundle
4. poi digitare `ent bundle publish` che provvederà a creare e pubblicare l'immagine docker su un repository Docker. N.B. un account docker è fondamentale per la riuscita della pubblicazione del bundle 
5. poi digitare `ent bundle deploy`  che renderà disponibile il bundle nel portale Entando pronto per l'installazione tramite ``HUB`` 
6. digitare infine `ent bundle install` se si vuole installare direttamente il bundle senza passare per l' ``HUB``

## Installazione del bundle tramite Entando HUB (Utente normale)

1. Accedi alla tua istanza di Entando
2. Vai su `Hub` e cliccaci sopra 
3. Vedrai una lista di ``bundles`` che posso essere installati
4. Fare clic sul pulsante ``Distribuito`` su ``pom-gallerie``
5. Non appena la finestra modale si apre cliccare sul bottone ``installa``
6. Un'altra finestra modale si aprirà in questo caso cliccate prima sul bottone ``aggiorna tutto`` e sul bottone ``ok``
7. Aspetta che il ``bundle`` termini l'installazione
8. Chiudere pure la finestra modale in caso non si siano evidenziati errori durante l'installazione
9. Utilizzate il widget in ogni pagina ove sia necessario o modificatelo in base alle necessità progettuali.

# Approfondimenti (developers)

## Installazione della CLI 
```https://developer.entando.com/next/docs/getting-started/entando-cli.html#install-the-cli```

## Gestione bundle e comandi della CLI 
```https://developer.entando.com/next/docs/curate/bundle-details.html#entando-bundle-conventions```

## Struttura del bundle 
```https://developer.entando.com/next/docs/curate/bundle-details.html```

## Hub configurazione e installazione
Per comprendere a fondo come utilizzare Hub, fate riferimento a ```https://developer.entando.com/v7.0/tutorials/solution/entando-hub.html#overview```

