**Descrizione**
------------------
__Attenzione!__ Le istruzioni sotto riportate fanno riferimento a una versione di __Entando 5__ non più supportata.  Si rimanda pertanto [all’ultima versione della documentazione disponibile](../README.md)

Questo progetto è caratterizzato dalla collaborazione tra il Comune di Cagliari con il Team Digitale e AGID. Lo scopo è quello di rendere disponibili in *White Label* componenti del portale del Comune in modo che tutte le Pubbliche Amministrazioni possano procedere al riuso. 

In questa prima fase sono stati messi a disposizione cinque modelli di pagina e otto tipi di contenuto. Nel corso del tempo questa selezione verrà regolarmente arricchita con nuovi elementi e componenti.

**Struttura del repository**
---------------------------------------

All'interno del repository sono presenti:

 - una cartella immagini con gli screenshot più significativi del portale
 - una cartella plugin, contenente la parte applicativa basata su Entando, ovvero gli attributi custom utilizzati nei tipi di contenuto
 - i template statici html + CSS
 - i template grafici in formato sketch, font e immagini utilizzati per la UI 
 - una cartella con i bundle della UI contenente i modelli di pagina e i tipi di contenuto riusabili

**Installazione**
-------------------
Attualmente per l'installazione è necessario utilizzare una versione Beta di Entando chiamata 5.3.0-SNAPSHOT, la quale lascerà presto spazio ad una nuova release che snellirà ulteriormente questa procedura.  

Per prima cosa occorre scaricare il *core* di Entando nella suddetta versione dal repository di github all'url [https://github.com/entando/entando-core/tree/v5.3.0-dev](https://github.com/entando/entando-core/tree/v5.3.0-dev), verificare di essere nella branch v5.3.0-dev e fare il download del progetto cliccando sulla freccia in basso del pulsante `Code` e selezionando `Download ZIP` . 

Successivamente si devono scaricare, nello stesso modo e nella stessa versione, anche *entando-components* ([https://github.com/entando/entando-components/tree/v5.3.0-dev](https://github.com/entando/entando-components/tree/v5.3.0-dev)) e *entando-archetypes* ([https://github.com/entando/entando-archetypes/tree/v5.3.0-dev](https://github.com/entando/entando-archetypes/tree/v5.3.0-dev)). Dopo aver estratto i progetti, installare queste componenti in locale eseguendo via terminale, dalla root di ogni progetto, il comando `mvn clean install -DskipTests`. 

Secondariamente, si dovrà scaricare dal repository del Comune di Cagliari il progetto "componentiPortale" ([https://gitlab.com/comunedicagliari/componentiportale](https://gitlab.com/comunedicagliari/componentiportale)) ed eseguire il medesimo comando di installazione. 

Infine non rimane che creare, in una nuova directory, ex novo un nuovo progetto con il comando `mvn archetype:generate -Dfilter=entando`. Seguire le istruzioni di installazione e scegliere di creare una Entando web App premendo il numero corrispondente a questa scelta `Entando sample web app Archetype: an agile, modern and user-centric open source web app like platform`, nella versione `5.3.0-SNAPSHOT`e nome, ad esempio: 'miocomune'.


**Utilizzo**
---------------------
Ora che sono stati installati e creati tutti gli elementi necessari, dalla root del proprio progetto, eseguire il comando `mvn clean jetty:run` per testare che il portale vuoto, che abbiamo appena istanziato, stia funzionando. Se il portale funziona correttamente, possiamo installare gli elementi desiderati tra quelli a disposizione per il riuso.

Entriamo nella cartella ‘miocomune’ ed incolliamo all’interno del file ‘POM.XML’ le *dependency* per i modelli di contenuto e modelli di pagina che desideriamo inserire nel progetto. Di seguito il codice delle varie *dependency* disponibili.

Modelli di contenuto:
```
<dependency>
	<artifactId>entando-content-whitelabel-luogo</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-news</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
    <type>war</type>
    </dependency>
<dependency>
	<artifactId>entando-content-whitelabel-argomento</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-documento</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-evento</artifactId>	
	<groupId>org.entando.entando.bundles.contents</groupId>
    <version>${entando.version}</version>
    <type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-organizzazione</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-persona</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-content-whitelabel-servizio</artifactId>
	<groupId>org.entando.entando.bundles.contents</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
```
Modelli di pagina:
```
<dependency>
	<artifactId>entando-page-whitelabel-dettaglio</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-page-whitelabel-argomento</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-page-whitelabel-areapersonale</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
<dependency>
	<artifactId>entando-page-whitelabel-sezioniprincipali</artifactId>
	<groupId>org.entando.entando.bundles.page</groupId>
	<version>${entando.version}</version>
	<type>war</type>
</dependency>
```

I nuovi elementi installati saranno ora disponibili sul portale. I modelli di contenuto saranno visibili dell'admin console del progetto nel tab CMS sotto la voce ‘content types’. Mentre sarà possibile visionare i modelli di pagina dal tab UX Patten sotto la voce ‘page models’.

Una volta completata l’inclusione dei nostri elementi *White Label*, possiamo avere a che fare con due tipi di scenari:

1. il nuovo progetto ‘miocomune’ è vuoto ed è senza pagine. In questo caso si creeranno le nuove pagine direttamente con gli id che il footer si aspetta di default:
		 
	 - 'amministrazione_trasparente'
	 - 'posta_elettronica_certificata'
	 - 'organizzazione'
	 - 'scrivi_comune'
	 - 'rubrica'
	 - 'rss'

2.  le pagine referenziate nel *footer* esistono già, ma con altro codice. Qualora si stiano utilizzando degli elementi già esistenti, sarà necessario modificare il fragment di riferimento (‘cagliari_template_footer_info’) variando gli assign con i codici delle pagine esistenti nel proprio progetto. 
	L’elenco dei codici di pagina referenziati nel footer sono:
	```
	<#assign ammTraspPageIdVar='amministrazione_trasparente'>
	<#assign certEmailPageIdVar='posta_elettronica_certificata'>
	<#assign organizzazionePageIdVar='organizzazione'>
	<#assign scriviComunePageIdVar='scrivi_comune'>
	<#assign rubricaDipendentiPageIdVar='rubrica'>
	<#assign rssPageIdVar='rss'>
	```

	Se, ad esempio, nel progetto locale la pagina di amministrazione trasparente ha codice 'ammTrasp', la relativa dichiarazione deve essere modificata in `<#assign ammTraspPageIdVar='ammTrasp'>`

Qualora si voglia creare un tipo di contenuto "Organizzazione" ed inserirlo nel *footer* per mostrare i relativi dati di pertinenza, sarà necessario rimuovere il commento dall'assegnamento `<#-- assign organizzazioneContentIdVar='' -->` per renderlo attivo e specificare l'id del contenuto. Ad esempio, se il contenuto relativo all’organizzazione è CNG117, la riga dovrà diventare: 
`<#assign organizzazioneContentIdVar='CNG117'>`

Lo stesso procedimento si deve applicare nel ‘cagliari_template_head’. Una volta ricercato il fragment è obbligatorio cambiare il valore di default della variabile con il nome della propria webApp: `<#assign webappNameVar='miocomune'>`. Ad esempio, se il mio progetto è raggiungibile all’indirizzo "localhost:8080/villamarina", la variabile deve essere così definita: `<#assign webappNameVar='villamarina'>`.

Un ultimo *fragment* da ricercare e modificare è 'cagliari_template_search_modal'. Questo *fragment* in uso all’*header* costruisce un link alla pagina individuata tramite il codice del widget che mostra i risultati di una ricerca; il codice di default del widget è 'search_result', quindi, qualora si volesse personalizzare il codice del widget, è necessario modificare il seguente assigment `<#assign searchResultWidgetCode='search_result'>` in `<#assign searchResultWidgetCode='custom_search_result'>` dove 'custom_search_result' è il codice del widget modificato.

--------------------------------------------
Per maggiori approfondimenti sulla piattaforma visitate la sezione [documentazione di Entando](https://dev.entando.org/old-version/old-version.html) che contiene tutte le risorse ed informazioni dettagliate per gli sviluppatori.
