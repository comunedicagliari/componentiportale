**Descrizione**

Il progetto nasce dalla collaborazione tra il Comune di Cagliari, il Team Digitale e AGID, con l’ obiettivo di favorire lo scambio, la condivisione ed il riuso di software tra Pubbliche Amministrazioni. Nel corso degli anni il Comune ha provveduto ad aggiornare il seguente repository, arricchendolo con nuovi componenti  White Label  che sfruttano le funzionalità evolute di componibilità della piattaforma Entando (su cui è basato il portale). Questi componenti sono disponibili gratuitamente per tutte le municipalità che intendono utilizzarli.


**Struttura del repository**

All'interno del repository sono presenti i seguenti componenti, disponibili per l’immediato riuso:

- una cartella immagini con gli screenshot più significativi del portale
- i template statici html + CSS
- i template grafici in formato sketch, font e immagini utilizzati per la UI
- una cartella __whitelabel-2023__ con i principali componenti del portale Comune Cagliari organizzati in bundle ( prerequisito: _Entando 7.x_)
